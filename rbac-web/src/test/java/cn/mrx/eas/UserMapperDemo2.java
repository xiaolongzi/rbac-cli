package cn.mrx.eas;

import cn.mrx.eas.dao.sys.SysUserMapper;
import cn.mrx.eas.service.sys.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Author: Mr.X
 * Date: 2017/12/6 下午3:06
 * Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext-service.xml", "classpath:spring/applicationContext-dao.xml"})
@Slf4j
public class UserMapperDemo2 {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Test
    public void test01() {
        //log.info("根据主键获取用户信息==>{}", sysUserMapper.selectByPrimaryKey(1));
    }
}
