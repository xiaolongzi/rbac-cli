package cn.mrx.eas.service.sys;

import cn.mrx.eas.dto.sys.SysPermission;
import cn.mrx.eas.server.BSGrid;
import cn.mrx.eas.server.ServerResponse;
import cn.mrx.eas.vo.sys.TreeNode;

import java.util.List;

/**
 * Author: Mr.X
 * Date: 2017/12/24 下午2:51
 * Description:
 */
public interface ISysPermissionService {

    BSGrid permissionListPage(Integer curPage, Integer pageSize, String name);

    SysPermission findOneById(Integer id);

    List<TreeNode> allNodes();

    ServerResponse editPermission(SysPermission sysPermission);

    ServerResponse addPermission(SysPermission sysPermission);

    ServerResponse batchDel(String ids);

    List<SysPermission> findAll();

    List<SysPermission> findAllByUserId(Integer userId);
}