package cn.mrx.eas.service.sys;

import cn.mrx.eas.dto.sys.SysRole;
import cn.mrx.eas.server.BSGrid;
import cn.mrx.eas.server.ServerResponse;
import cn.mrx.eas.vo.sys.TreeNode;

import java.util.List;

/**
 * Author: Mr.X
 * Date: 2017/12/24 上午11:41
 * Description:
 */
public interface ISysRoleService {

    BSGrid roleListPage(Integer curPage, Integer pageSize);

    SysRole findOneById(Integer id);

    List<TreeNode> allNodes(Integer roleId);

    ServerResponse editRole(SysRole sysRole, String permissionIds);

    ServerResponse addRole(SysRole sysRole, String permissionIds);

    ServerResponse batchDel(String ids);

    List<SysRole> findAllByUserId(Integer userId);
}